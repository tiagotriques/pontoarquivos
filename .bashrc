export EDITOR=/usr/bin/vim
export GOPATH=$HOME/go
export PATH=$PATH:$GOROOT/bin:$GOPATH/bin:$HOME/.local/bin
export PATH=$PATH:/opt/android-sdk/build-tools/30.0.2

export _JAVA_AWT_WM_NONREPARENTING=1

export ANDROID_SDK_ROOT=/home/triques/Android

# Esp8266 stuff
export IDF_PATH=~/esp/ESP8266_RTOS_SDK
export PATH="$PATH:$HOME/esp/xtensa-lx106-elf/bin"

# vi keybindings in bash
set -o vi
bind -m vi-insert "\C-l":clear-screen

# Man colors
export LESS_TERMCAP_mb=$'\e[1;32m'
export LESS_TERMCAP_md=$'\e[1;32m'
export LESS_TERMCAP_me=$'\e[0m'
export LESS_TERMCAP_se=$'\e[0m'
export LESS_TERMCAP_so=$'\e[01;33m'
export LESS_TERMCAP_ue=$'\e[0m'
export LESS_TERMCAP_us=$'\e[1;4;31m'

alias :q='exit'
alias cba='$EDITOR ~/.bashrc'
alias chmox='chmod +x'
alias cmp='$EDITOR ~/.mpd/mpd.conf'
alias cnc='$EDITOR ~/.ncmpcpp/config'
alias cvi='$EDITOR ~/.vimrc'
alias htb='unzip -P hackthebox'
alias la='ls --group-directories-first -A'
alias ll='ls --group-directories-first -alF'
alias ls='ls --group-directories-first --color=auto'
alias py2='python2'
alias py='python3'
alias sb='. ~/.bashrc'
alias shus='simple-http-upload-server'
alias sr='speedread'
alias sv='sudo $EDITOR '
alias tb="nc termbin.com 9999"
alias v='$EDITOR'
alias vp='$EDITOR -p'
alias vim='$EDITOR'
alias ncmpcpp='ncmpcpp-ueberzug'

# git alias
alias gs='git status'
alias ga='git add'
alias gp='git push'
alias gc='git commit'

# Functions
cd() {
	builtin cd "$@" && ls;
}

# Roubado do connermcd
pacs() {
    doas pacman -Sy $(pacman -Ssq | fzf -m --preview="pacman -Si {}" --preview-window=:hidden --bind=space:toggle-preview)
}

remove() {
    doas pacman -Rns $(pacman -Qq | fzf -m --preview="pacman -Si {}" --preview-window=:hidden --bind=space:toggle-preview)
}

docx2txt() {
	unzip -p "$1" word/document.xml | sed -e 's/<\/w:p>/\n/g; s/<[^>]\{1,\}>//g; s/[^[:print:]\n]\{1,\}//g'
}

c() {
    curl cheat.sh/$1
}

parse_git_branch() {
     git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1)/'
}

export PS1="\u@\h \[\e[32m\]\w \[\e[91m\]\$(parse_git_branch)\[\e[00m\]$ "

pullapk() {
	apk=$(adb shell pm list packages | awk -F':' '{print $2}' | fzf)
	test -d "$apk" || mkdir "$apk"
	cd "$apk" || return

	while read -r path; do
		adb pull "$path"
	done <<< "$(adb shell pm path "$apk" | awk -F':' '{print $2}')"
}
