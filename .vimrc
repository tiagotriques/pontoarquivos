"{{{ Plugins
	call plug#begin('~/.vim/plugged')
		Plug 'godlygeek/tabular'
		Plug 'beautify-web/js-beautify'
		Plug 'junegunn/fzf', { 'do': './install --bin' }
		Plug 'junegunn/fzf.vim'
		Plug 'dart-lang/dart-vim-plugin'
		Plug 'vimwiki/vimwiki'
		Plug 'morhetz/gruvbox'
		Plug 'neoclide/coc.nvim', {'branch': 'release'}
		Plug 'tpope/vim-fugitive'
		Plug 'yaroot/vissort'
		Plug 'mattn/emmet-vim'
	call plug#end()
"}}}
"{{{ Status Line
" Taken from makc.co
	set statusline=                 " Start with a clean statusline
	set statusline+=%#IncSearch#    " Some random color
	set statusline+=\ %y            " Filetype
	set statusline+=\ %r            " Space
	set statusline+=%#CursorLineNr# " IDK these colors
	set statusline+=\ %F            " Full path to open file
	set statusline+=%=              " Right side settings
	set statusline+=%#Search#       " This one I know - it's Yellow :)
	set statusline+=\ %l/%L         " Current line / total
	set statusline+=\ [%c]          " Column
"}}}
"{{{ Colorscheme
	colorscheme gruvbox
"}}}
"{{{ Random Stuff
	set hidden
	set nowritebackup
	set nobackup
	let mapleader="ç"
	set nocompatible
	filetype plugin indent on
	syntax enable
	noremap <A-c> "+y
	noremap <A-v> "+p
	noremap <A-u> viwu
	noremap <A-U> viwU
	vnoremap <A-c> "+y
	vnoremap <A-v> "+p
	vnoremap <A-u> viwu
	vnoremap <A-U> viwU
	set autoindent   " Auto indent
	set autoread     " Auto read file on external change
	set hlsearch     " Highlight search
	"set ignorecase   " Case insensitive searching
	set incsearch    " Incremental search
	set lazyredraw   " Do not update when not needed
	set mouse=a
	set noswapfile   " Disable swap file
	set nowrap       " Do not wrap lines
	set shiftwidth=4 " Back to 4 spaces again
	set tabstop=4    " Same
	set magic
	set nowb
	set showmatch
	set smartcase
	set smartindent
	set shortmess-=S " Shows the number of matches, AFAIK
"}}}
"{{{ Completion

" What
set updatetime=100

" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

vmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)

" Symbol renaming.
nmap <leader>rn <Plug>(coc-rename)

" Use K to show documentation in preview window.
"nnoremap <silent> K :call <SID>show_documentation()<CR>
inoremap <silent><expr> <c-space> coc#refresh()

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  elseif (coc#rpc#ready())
    call CocActionAsync('doHover')
  else
    execute '!' . &keywordprg . " " . expand('<cword>')
  endif
endfunction

autocmd CursorHold * silent call CocActionAsync('highlight')

"}}}
"{{{ Movement
" Visually move block
	vnoremap J :m '>+1<CR>gv=gv
	vnoremap K :m '<-2<CR>gv=gv

" Visually go down a line
	noremap j gj
	noremap k gk

" Disable arrow keys
	noremap <Up> <NOP>
	noremap <Down> <NOP>
	noremap <Left> <NOP>
	noremap <Right> <NOP>

"}}}
"{{{ Even more random stuff idk where to put
	noremap <leader><leader> :noh<CR>
	set splitbelow splitright
	"autocmd BufWritePre * %s/\s\+$//e
	map <F9> :Files<CR>
	set foldmethod=marker
	set nu
	set rnu

	let g:vimwiki_customwiki2html = '/home/triques/.vim/plugged/custom/customwiki2html.sh'
	let g:vimwiki_list = [{'path': '~/Documentos/vimwiki/',
                      \ 'syntax': 'markdown', 'ext': '.md'}]
"}}}
