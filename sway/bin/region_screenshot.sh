#!/bin/sh

filename=$(mktemp --suffix=.png)
slurp | grim -g - "$filename"
wl-copy < "$filename"
