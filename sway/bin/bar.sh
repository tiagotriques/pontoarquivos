#!/bin/sh

print_date() {
	date +'%d/%m/%Y %l:%M %p'
}

print_ip() {
	ip route get 1.1.1.1 2> /dev/null | awk '{print $7}'
}

print_bat() {
	acpi | grep -v unavailable | grep -o '[0-9]\+%'
}

print_vol() {
	amixer get Master | tail -1 | grep -o '[0-9]\+%'
}

while :; do
	echo "IP:" "$(print_ip)" "Vol:" "$(print_vol)" "$(print_date)" "Bat:" "$(print_bat)"
	sleep 1
done
